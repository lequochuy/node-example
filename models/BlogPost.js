const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const BlogSchema = new Schema({
    title: String,
    body: String,
    username: String,
    datePosted: {
        type: Date,
        default: new Date()
    },
    image: String
});
//Exportdd
const Blog = mongoose.model('Post',BlogSchema);
module.exports = Blog