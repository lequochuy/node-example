const Post = require('../models/BlogPost.js')
module.exports = (req, res) => {
  Post.findById(req.params.id, (error, detailPost) => {
    res.render('post', {
      detailPost
    })
  })
}