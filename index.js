const express = require('express')
const app = express()
const mongoose = require('mongoose')
const expressSession = require('express-session');
mongoose.connect('mongodb://localhost/blog', {useNewUrlParser: true, useUnifiedTopology: true})
mongoose.set('useCreateIndex', true);
const ejs = require('ejs')
const bodyParser = require('body-parser')
const fileUpload = require('express-fileupload')
app.use(fileUpload())
app.use(bodyParser.json())
app.use(bodyParser.json({ type: 'application/json' }))
app.use(bodyParser.urlencoded({extended:true}))
app.set('view engine', 'ejs')
app.use(expressSession({
  secret: 'keyboard cat',
  proxy : true, 
  resave : true, 
  saveUninitialized : true 
}))

const newPostController = require('./controllers/newPost')
const homeController = require('./controllers/home')
const storePostController = require('./controllers/storePost')
const getPostController = require('./controllers/getPost')
const newUserController = require('./controllers/newUser')
const storeUserController = require('./controllers/storeUser')
const loginController = require('./controllers/login')
const loginUserController = require('./controllers/loginUser')
const authMiddleware = require('./middleware/authMiddleware')
const logoutController = require('./controllers/logout')

global.loggedIn = null;
app.use("*", (req, res, next) => {
  loggedIn = req.session.userId;
  next()
});

app.use(express.static('public'))

app.listen(5000, () => {
  console.log('APP listening')
})

// const customMiddleWare = (req, res, next) => {
//   console.log('Custom middle ware called')
//   next()
// }
// app.use(customMiddleWare)

const validateMiddleware = require("./middleware/validationMiddleware");
const redirectIfAuthenticatedMiddleware = require('./middleware/redirectIfAuthenticatedMiddleware')
app.use('/posts/store', validateMiddleware)

app.get('/', homeController)

app.get('/posts/new', authMiddleware, newPostController)

app.get('/post/:id', getPostController)

app.post('/posts/store', authMiddleware, storePostController)

app.get('/auth/register', redirectIfAuthenticatedMiddleware, newUserController)
app.post('/users/register', redirectIfAuthenticatedMiddleware, storeUserController)
app.get('/auth/login', redirectIfAuthenticatedMiddleware, loginController)
app.post('/users/login', redirectIfAuthenticatedMiddleware, loginUserController)
app.get('/auth/logout', logoutController)
const Book = require('./models/Book');
app.get('/test', async (req, res) => {
  const books = await Book.findOne({ title: 'Test' }).populate('author', 'username')
  res.json(books)
})
app.use((req, res) => res.render('notfound'));