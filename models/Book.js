const mongoose = require('mongoose')
const User = require('./User')
const Schema = mongoose.Schema;
const Book = new Schema({
    title: String,
    published:{
        type: Date,
        default: Date.now
    },
    keywords: Array,
    pulished: Boolean,
    author: {
        type: Schema.ObjectId,
        ref: 'User'
    },
    detail:{
        modelNumber: Number,
        hardcoever: Boolean,
        reviews: Number,
        rank: Number
    }
});
//Exportdd
const Blogbook = mongoose.model('Book',Book);
module.exports = Blogbook