const Post = require('../models/BlogPost.js')

module.exports = (req, res) => {
  Post.find({}, (error, posts) => {
    res.render('index', {
      blogposts: posts
    })
  })
}